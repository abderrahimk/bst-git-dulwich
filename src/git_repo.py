from dataclasses import dataclass
from enum import Enum
import os

from buildstream import Source, SourceError
from buildstream.utils import url_directory_name

from dulwich.repo import Repo
from dulwich.client import get_transport_and_path

from ._utils import git_describe, REF_REGEX
from ._utils import pattern_to_regex, version_sort_key
from ._utils import stage_repo

from ._common import GitMirror


class RefFormat(Enum):
    SHA1 = "sha1"
    GIT_DESCRIBE = "git-describe"


@dataclass
class RepoInfo:
    primary: bool
    ref: str
    url: str


class GitSource(Source):
    BST_MIN_VERSION = "2.0"

    def configure(self, node):
        CONFIG_KEYS = ["ref", "url", "track", "exclude", "ref-format"]

        node.validate_keys(Source.COMMON_CONFIG_KEYS + CONFIG_KEYS)
        self.ref = None
        self.load_ref(node)

        self.url = node.get_str("url")
        self.mark_download_url(self.url)

        if self.url.endswith(".git"):
            norm_url = self.url[:-4]
        else:
            norm_url = self.url

        self.mirror_dir = os.path.join(
            self.get_mirror_directory(),
            url_directory_name(norm_url) + ".git",
        )

        self.tracking = node.get_str("track", None)
        self.exclude = node.get_str_list("exclude", [])

        self.ref_format = node.get_enum("ref-format", RefFormat, RefFormat.SHA1)

        # make the url-manifest script happy
        self.mirror = RepoInfo(primary=True, ref=self.ref, url=self.url)

    def preflight(self):
        pass

    def get_unique_key(self):
        return {"ref": self.ref}

    # loading and saving refs
    def load_ref(self, node):
        if "ref" not in node:
            return
        ref = node.get_str("ref")
        if REF_REGEX.match(ref) is None:
            raise SourceError(f"ref {ref} is not in the expected format")
        self.ref = ref

    def get_ref(self):
        return self.ref

    def set_ref(self, ref, node):
        self.ref = ref
        node["ref"] = ref

    def is_cached(self):
        tag, _, sha = REF_REGEX.match(self.ref).groups()

        with Repo(self.mirror_dir, bare=True) as repo:
            cached = sha.encode() in repo
            if tag:
                ref = b"refs/tags/" + tag.encode()
                cached_ref = ref in repo

                cached = cached and cached_ref

        return cached

    def track(self):
        if not self.tracking:
            return

        url = self.translate_url(self.url)
        self.status(f"Tracking {self.tracking} from {url}")
        client, path = get_transport_and_path(url)
        refs_dict = {k.decode(): v.decode() for k, v in client.get_refs(path).items()}

        real_refs = {ref for ref in refs_dict if not ref.endswith("^{}")}
        matching_regex = pattern_to_regex(self.tracking)

        matching_refs = [ref for ref in real_refs if matching_regex.match(ref)]

        if self.exclude:
            exclude_regexs = [pattern_to_regex(pattern) for pattern in self.exclude]
            matching_refs = [
                ref
                for ref in matching_refs
                if not any(regex.match(ref) for regex in exclude_regexs)
            ]

        self.debug("Refs to be tracked", detail="\n".join(matching_refs))

        ref = max(matching_refs, key=version_sort_key)

        # peel the ref if possible
        peeled_ref = ref + "^{}"
        if peeled_ref in refs_dict:
            resolved = refs_dict[peeled_ref]
        else:
            resolved = refs_dict[ref]

        self.status(f"Tracked {ref}: {resolved}")

        if self.ref_format == RefFormat.SHA1:
            return resolved

        if "tags" in ref:
            tag = ref.split("/", 2)[-1]
            return f"{tag}-0-g{resolved}"

        # Need to fetch to generate the ref in git-describe format
        fetcher = GitMirror(self, self.url, resolved)
        fetcher.fetch()

        with Repo(self.mirror_dir) as repo:
            return git_describe(repo, resolved)

    def get_source_fetchers(self):
        yield GitMirror(self, self.url, self.ref)

    def stage(self, directory):
        tag, _, sha = REF_REGEX.match(self.ref).groups()

        stage_repo(self.mirror_dir, tag, sha, directory, self)


def setup():
    return GitSource
