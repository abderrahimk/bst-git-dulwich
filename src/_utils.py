import fnmatch
import re

from stat import S_ISDIR

from dulwich.repo import Repo
from dulwich.objects import Commit, Tag

REF_REGEX = re.compile(r"(?:(.*)(?:-(\d+)-g))?([0-9a-f]{40})")
SPLITTER_REGEX = re.compile(r"[a-zA-Z]+|[0-9]+")


def pattern_to_regex(pattern):
    """
    Transforms a glob pattern into a regex to match refs.

    If the pattern doesn't start with "refs/", it will be considered to only match refs in
    refs/tags/ and refs/heads.
    """
    if pattern.startswith("refs/"):
        return re.compile(fnmatch.translate(pattern))

    return re.compile("refs/(heads|tags)/" + fnmatch.translate(pattern))


def version_sort_key(elt):
    """
    A sort key that can be used to versions. It sorts letters before numbers (so 1.beta is earlier
    than 1.0) and disregards separators (so 1.beta, 1~beta and 1beta are the same).
    """
    return [
        # to sort letters before digits
        (-1, part) if part.isalpha() else (int(part), "")
        for part in SPLITTER_REGEX.findall(elt)
    ]


def init_repo(mirror_dir):
    """
    Open a repo at the given mirror_dir, creating an empty bare repo if it doesn't exist.
    """
    try:
        return Repo.init_bare(mirror_dir, mkdir=True)
    except FileExistsError:
        return Repo(mirror_dir, bare=True)


def stage_repo(mirror_dir, tag, sha, directory, log):
    """
    Stage a checkout of the repository at mirror_dir, with a minimal repo that is just enough for
    common operations (like `git describe --dirty` and `git log .`).

    sha is the commit sha to stage and tag is the tag that should be staged so that `git describe`
    is relative to it (could point to the same commit).
    log is the logging context (e.g. bst plugin).
    """

    with init_repo(mirror_dir) as repo, log.timed_activity(f"Checking out {sha} from git"):
        commit = repo.get_object(sha.encode())
        commit_id = commit.id.decode()

        # Compatibility: some older git plugins can generate refs where the sha isn't a commit
        # if isinstance(commit, Tag):
        #     log.warn(f"{commit_id} points to a tag, not to a commit")
        #     commit = repo.get_object(commit.object[1])

        log.status(f"Checking out {commit_id}")

        objects = {}
        if tag:
            tag_ref = f"refs/tags/{tag}".encode()
            tag_obj = repo[tag_ref]

            if tag_obj.type_name == b"tag":
                objects[tag_obj.id] = tag_obj
                base = repo[tag_obj.object[1]]
            else:
                base = tag_obj
        else:
            base = commit

        # I've seen this once at least: a tag that points to a tag, but we don't go recursing
        # if isinstance(base, Tag):
        #     base = repo.get_object(base.object[1])

        assert isinstance(
            base, Commit
        ), f"Tag {tag} does not point to a commit (type {type(tag_obj)}"

        log.debug(f"Staging commit {commit.id.decode()}, base {base.id.decode()}")

        if commit == base:
            objects[commit.id] = commit
        else:
            for entry in repo.get_walker(commit.id, base.parents):
                objects[entry.commit.id] = entry.commit

        # The shallow bases
        shallow = [base.id]

        # All the tree objects. `git log .` and `git describe --dirty` need them
        trees = []

        def collect_trees(repo, root_sha):
            trees.append(root_sha)

            for _, mode, sha in repo[root_sha].items():
                if S_ISDIR(mode):
                    collect_trees(repo, sha)

        for obj in objects.values():
            if not isinstance(obj, Commit):
                continue

            if not all(p in objects for p in obj.parents):
                shallow.append(obj.id)

            collect_trees(repo, obj.tree)

        for tree in trees:
            objects[tree] = repo[tree]

        with Repo.init(directory) as dest:
            log.status(f"Adding {len(objects)} objects")
            for obj in objects.values():
                dest.object_store.add_object(obj)

            if tag:
                dest.refs[tag_ref] = tag_obj.id

            dest.refs[b"HEAD"] = commit.id
            dest.update_shallow(shallow, [])

        # checkout
        with Repo(directory, object_store=repo.object_store) as dest:
            dest.reset_index(commit.tree)


def git_describe(repo, revision):
    """
    Returns the equivalent of `git describe --tags --abbrev=40`
    """
    # Get a list of all tags
    tags = repo.refs.as_dict(b"refs/tags")

    if not tags:
        return revision

    commit_tags = {}
    for tag_name, tag_sha in list(tags.items()):
        commit = repo.get_object(tag_sha)
        while isinstance(commit, Tag):
            commit = repo.get_object(commit.object[1])

        commit_tags[commit.id] = tag_name.decode()

    count = 0

    walker = repo.get_walker([revision.encode()])
    for entry in walker:
        commit_id = entry.commit.id
        if commit_id in commit_tags:
            return f"{commit_tags[commit_id]}-{count}-g{revision}"

        count += 1

    return revision
