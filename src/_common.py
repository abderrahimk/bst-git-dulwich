import threading
from buildstream import SourceFetcher, SourceError

from dulwich.client import get_transport_and_path
from dulwich.errors import GitProtocolError
from dulwich.refs import ANNOTATED_TAG_SUFFIX

from ._utils import init_repo, REF_REGEX


LOCKS = {}


class GitMirror(SourceFetcher):
    def __init__(self, source, url, ref):
        super().__init__()
        self.mark_download_url(url)

        self.source = source
        self.url = url

        tag, depth, sha = REF_REGEX.match(ref).groups()

        self.sha = sha
        self.tagref = f"refs/tags/{tag}".encode() if tag else None
        self.depth = int(depth) + 1 if depth else None

    def fetch(self, alias_override=None):
        url = self.source.translate_url(self.url, alias_override=alias_override)
        lock = LOCKS.setdefault(self.source.mirror_dir, threading.Lock())

        with lock, init_repo(self.source.mirror_dir) as repo, self.source.timed_activity(
            f"Fetching from {url}"
        ):
            if self.sha.encode() in repo and (not self.tagref or self.tagref in repo.refs):
                return

            self.source.status(f"Fetching {self.sha}")

            def wants(refs, depth=None):
                wanted = set()
                if self.tagref:
                    if self.tagref in refs:
                        wanted.add(refs[self.tagref])
                    else:
                        raise SourceError(f"ref {self.tagref.decode()} not found in remote {url}")

                if self.sha.encode() in refs.values():
                    wanted.add(self.sha.encode())
                else:
                    # we want everything
                    self.source.warn(f"No ref matches {self.sha}, downloading everything.")
                    wanted.update(refs.values())

                return wanted

            client, path = get_transport_and_path(url)

            try:
                remote_refs = client.fetch(path, repo, determine_wants=wants, depth=self.depth)
            except GitProtocolError as e:
                raise SourceError(f"failed to fetch: {e}") from e
            except Exception as e:
                # should be more specific
                raise SourceError(f"failed to fetch: {e}") from e

            # check that we actually pulled the required commit
            if self.sha.encode() not in repo:
                raise SourceError(f"{self.sha} not found in remote {url}")

            if self.tagref:
                repo.refs.add_if_new(self.tagref, remote_refs[self.tagref])
                return

            local_refs = repo.get_refs()

            extra_wants = []
            extra_haves = []
            for ref in remote_refs:
                if not ref.startswith(b"refs/tags"):
                    continue
                if ref.endswith(ANNOTATED_TAG_SUFFIX):
                    continue
                if ref in local_refs:
                    continue
                if remote_refs[ref] in repo:
                    repo.refs.add_if_new(ref, remote_refs[ref])
                else:
                    peeled_ref = ref + ANNOTATED_TAG_SUFFIX
                    if peeled_ref in remote_refs and remote_refs[peeled_ref] in repo:
                        extra_haves.append(remote_refs[peeled_ref])
                        extra_wants.append(remote_refs[ref])
            if extra_wants:
                self.source.status(f"Fetching {len(extra_wants)} extra tags")
                f, commit, abort = repo.object_store.add_pack()
                try:
                    walker = repo.get_graph_walker(extra_haves)
                    remote_refs = client.fetch_pack(path, lambda _: extra_wants, walker, f.write)
                except Exception as e:
                    # should be more specific
                    abort()
                    raise SourceError(f"failed to fetch: {e}") from e

                commit()
                for ref, value in remote_refs.items():
                    if value in extra_wants:
                        repo.refs.add_if_new(ref, value)
